﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c04_dz17_Delegates
{
    public static class GetMaxClass
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : struct
        {
            T toReturn = new T();
            float max = 0;
            bool initMax = false;
            
            foreach (T temp in e)
            {
                float fl = getParameter((T)temp);
                if (!initMax)
                {
                    initMax = true;
                    max = fl;
                    toReturn = (T)temp;
                }

                if (fl >= max)
                {
                    max = fl;
                    toReturn = (T)temp;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Метод возвращает float
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="argss"></param>
        /// <returns></returns>
        public static float GetFloat<T>(T argss)
        {
            try
            {
                float fl = (float)Convert.ToDouble(argss);
                return fl;
            }
            catch
            {
                throw new Exception("Enable convert to float");
            }
        }

        public static void Run()
        {
            List<int> list = new() { 1, 5, 3};
            float retVal = list.GetMax<int>(GetFloat);
            Console.WriteLine(retVal);
        }
    }


}
