﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c04_dz17_Delegates
{
    public class FilesSearcher
    {
        string Path = String.Empty;
        DirectoryInfo dir;
        
        /// <summary>
        /// принудительная отмены поиска
        /// </summary>
        public bool Cancellation = false;
        /// <summary>
        /// событие найден файл
        /// </summary>
        public event EventHandler FileFound;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="path">путь к директории для поиска</param>
        public FilesSearcher(string path)
        {
            Path = path;
            dir = new DirectoryInfo(path);
        }

        /// <summary>
        /// старт поиска
        /// </summary>
        public void SearchStart()
        {
            FileInfo[] files = dir.GetFiles("*.*", SearchOption.AllDirectories);
            foreach (FileInfo file in files)
            {
                FileArgs fArgs = new FileArgs(file.FullName);
                FileFound.Invoke(this, fArgs);

                if (Cancellation)
                {
                    Console.WriteLine("Поиск остановлен");
                    break;
                }
            }
        }

        /// <summary>
        ///  класс аргументы события
        /// </summary>
        public class FileArgs : EventArgs
        {
            public string FileName;
            public FileArgs(string name)
            {
                FileName = name;
            }
        }
    }
}
