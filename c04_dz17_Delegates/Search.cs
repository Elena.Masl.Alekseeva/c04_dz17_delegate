﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static c04_dz17_Delegates.FilesSearcher;

namespace c04_dz17_Delegates
{
    public static class Search
    {
        public static void Run(string pathToSearch)
        {
            FilesSearcher fs = new FilesSearcher(pathToSearch);
            fs.FileFound += Fs_FileFound;
            fs.SearchStart();
        }

        private static void Fs_FileFound(object? sender, EventArgs e)
        {
            if (e is FileArgs)
            {
              Console.WriteLine("Найден файл. Путь {0}", ((FileArgs)e).FileName);
              Console.WriteLine("Хотите продолжить поиск? Y/N");
              string yn = Console.ReadLine();
                if (yn.ToLower() == "n")
                    ((FilesSearcher)sender).Cancellation = true;

            }
            
        }
    }
}
